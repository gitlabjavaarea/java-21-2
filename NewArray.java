/*
* Java Arrays learning 
* @version 1
* @autor Mykhail Aleksandr
*/
  public class NewArray {
	public static void main (String[] args){
		
		
		int [] arrayA = new int [100];
		for(int i = 0; i< arrayA.length; i++){
			arrayA[i] = i + 1;
			System.out.println(arrayA[i]);
		}
		
		int [] arrayB = new int [100];
		for(int i = 0; i< arrayB.length; i++){
			arrayB[i] = arrayA[i];
			System.out.println(arrayB[i]);
		}
		
		
	}	
}